<?php
add_action( 'init', 'db_acpt_post_types' );
function db_acpt_post_types() {
    $args = array(
        'has_archive' => false,
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical' => false
    );

    // CPT
    $testimonial = acpt_post_type( 'testimonial', 'testimonials', false, apply_filters( 'db_acpt_testimonial_args', $args ) );

    // Taxonomy
    // acpt_tax( 'group', 'groups', array( $testimonial ), false );
}

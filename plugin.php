<?php
/*
Plugin Name: Developer Bundle
Description: A collection of widgets and shortcodes for WordPress and Genesis Framework.
Version: 1.4.5
Author: SuperFastBusiness
Author URI: http://www.superfastbusiness.com
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt
Bitbucket Plugin URI: https://bitbucket.org/webdevsuperfast/developer-bundle
*/

// Add Widget Class
if ( !class_exists( 'MB_Widget' ) )
	include_once( plugin_dir_path( __FILE__ ) . 'lib/classes/widget-class.php' );

// Vafpress
if ( !class_exists( 'VP_AutoLoader' ) )
	require_once ( plugin_dir_path( __FILE__ ) . 'lib/vafpress-framework/bootstrap.php' );

// Advanced Custom Post Type
if ( !class_exists( 'acpt' ) )
	include( plugin_dir_path( __FILE__ ) . 'lib/acpt/init.php' );

// AQ Resize
if( !class_exists( 'Aq_Resize' ) )
    require_once( plugin_dir_path( __FILE__ ) . 'lib/classes/aq_resizer.php' );

// Shortcodes
require_once( plugin_dir_path( __FILE__ ) . 'lib/classes/shortcodes.php' );

// Widgets
$widgets = glob( plugin_dir_path( __FILE__ ) . 'lib/widgets/*.php', GLOB_NOSORT );
if ( is_array( $widgets ) ) {
	foreach ( $widgets as $widget ) {
		include_once $widget;
	}
}

// Functions
$functions = glob( plugin_dir_path( __FILE__ ) . 'lib/functions/*.php', GLOB_NOSORT );
if ( is_array( $functions ) ) {
	foreach ( $functions as $function ) {
		require_once $function;
	}
}

// Check if Genesis is installed
register_activation_hook( __FILE__, 'db_up_activation_check' );
function db_up_activation_check() {
	$theme_info = get_theme_data( TEMPLATEPATH . '/style.css' );

	// need to find a way to check active themes is MultiSites	- This does not work in new 3.1 network panel.
	if( basename( TEMPLATEPATH ) != 'genesis' ) {
		deactivate_plugins( plugin_basename( __FILE__ ) ); // Deactivate ourself
		wp_die( 'Sorry, you can\'t activate unless you have installed <a href="http://www.studiopress.com/themes/genesis">Genesis</a>' );
	}
}

// Scripts
add_action( 'admin_enqueue_scripts', 'db_widget_enqueue_scripts' );
function db_widget_enqueue_scripts( $hook ){
	//Get current page
	$current_page = get_current_screen();

	//Only load if we are not on the nav menu page - where some of our scripts seem to be conflicting
	if ( $current_page->base != 'nav-menus' ){
		// Image Upload
		wp_enqueue_media();
		wp_enqueue_script( 'widget-image-upload', plugin_dir_url( __FILE__ ) . 'assets/js/image-upload.js', array( 'jquery' ) );
	}
}

add_action( 'wp_enqueue_scripts', 'db_do_widget_scripts' );
function db_do_widget_scripts() {
	wp_register_style( 'db-css', plugin_dir_url( __FILE__ ) . 'assets/css/style.css' );
	wp_enqueue_style( 'db-css' );

	wp_register_script( 'db-owl-carousel-js', plugin_dir_url( __FILE__ ) . 'assets/js/owl.carousel.min.js', array( 'jquery' ), null, true );

	wp_register_script( 'db-easytabs-js', plugin_dir_url( __FILE__ ) . 'assets/js/jquery.easytabs.min.js', array( 'jquery' ), null, true );

	wp_register_script( 'db-js', plugin_dir_url( __FILE__ ) . 'assets/js/main.min.js', array( 'jquery' ), null, true );
}

// Metabox for Masonry Layout
add_action( 'after_setup_theme', 'db_widget_metaboxes' );
function db_widget_metaboxes() {
	if( !class_exists( 'VP_AutoLoader' ) )
        return;

	$mb1 = new VP_Metabox( array(
        'id' => 'db_post',
		'types' => array( 'post' ),
		'title' => __( 'Masonry Settings', 'starter' ),
		'context' => 'side',
		'priority' => 'default',
		'is_dev_mode' => false,
		'mode' => WPALCHEMY_MODE_EXTRACT,
		'prefix' => '_post_',
		'template' => array(
			array(
				'type' => 'select',
		        'name' => 'fld_masonry',
		        'label' => __( 'Brick Size', 'starter' ),
				'items' => array(
					array(
						'value' => 'size11',
						'label' => '1x1'
					),
					array(
						'value' => 'size12',
						'label' => '1x2'
					),
					array(
						'value' => 'size21',
						'label' => '2x1'
					),
					array(
						'value' => 'size22',
						'label' => '2x2'
					),
				),
				'default' => array(
					'size11'
				)
			)
		)
	) );
}

// Masonry Sizes
add_action( 'init', 'db_masonry_thumbnail' );
function db_masonry_thumbnail() {
	add_image_size( 'masonry-size11', 265, 265, true );
	add_image_size( 'masonry-size12', 265, 555, true );
	add_image_size( 'masonry-size21', 555, 265, true );
	add_image_size( 'masonry-size22', 555, 555, true );
}

// Add Class
add_filter( 'post_class', 'db_post_masonry_class' );
function db_post_masonry_class( $classes ) {
	$mason = get_post_meta( get_the_ID(), '_post_fld_masonry', true );
	// var_dump( $mason );

	if ( 'post' == get_post_type() ) {
		if ( $mason ) {
			$classes[] = $mason;
		} else {
			$classes[] = 'size11';
		}
	}
	return $classes;
}

// Add additional sidebar
add_action( 'widgets_init', 'db_do_add_sidebar' );
function db_do_add_sidebar() {
	register_sidebar( array(
		'name' => __( 'Tab Sidebar', 'starter' ),
		'id' => 'tab-sidebar',
		'description' => __( 'Place widgets you wish to display on tabs', 'starter' )
	) );
}

// Before Testimonial Content
function db_testimonial_before_content( $instance ) {
	do_action( 'db_testimonial_before_content', $instance );
}

// After Testimonial Content
function db_testimonial_content( $instance ) {
	do_action( 'db_testimonial_content', $instance );
}

// After Testimonial Content
function db_testimonial_after_content( $instance ) {
	do_action( 'db_testimonial_after_content', $instance );
}

// After Testimonial Loop
function db_testimonial_after_loop( $instance ) {
	do_action( 'db_testimonial_after_loop', $instance );
}

// Custom Image Function
function db_post_image() {
	global $post;
	$image = '';
	$image_id = get_post_thumbnail_id( $post->ID );
	$image = wp_get_attachment_image_src( $image_id, 'full' );
	$image = $image[0];
	if ( $image ) return $image;
	return db_get_first_image();
}

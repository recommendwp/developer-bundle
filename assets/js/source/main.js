(function($){
    $(window).load(function(){

        if ( $('.db_testimonials').length > 0 ) {
            $('.db_testimonials').each( function(index) {
                var instance = $(this).data('instance');
                testimonialInstance( instance );
            });

            function testimonialInstance( instance ) {
                var obj = window['testimonial' + instance];
                // console.log(obj);

                var sid = obj.id,
                    items = obj.items,
                    slidespeed = obj.slideSpeed,
                    paginationspeed = obj.paginationSpeed,
                    navigation = (obj.navigation == "true"),
                    pagination = (obj.pagination == "true"),
                    autoplay = (obj.autoPlay == "true"),
                    itemsdesktop = obj.itemsDesktop,
                    itemsdesktopsmall = obj.itemsDesktopSmall,
                    itemstablet = obj.itemsTablet,
                    itemsmobile = obj.itemsMobile,
                    autoheight = (obj.autoHeight == "true");

                var owl = $('#' + sid);

                owl.owlCarousel({
                    items: items,
                    slideSpeed: slidespeed,
                    paginationSpeed: paginationspeed,
                    navigation: navigation,
                    pagination: pagination,
                    autoPlay: autoplay,
                    autoHeight: autoheight,
                    navigationText: false,
                    itemsDesktop: itemsdesktop,
                    itemsDesktopSmall: itemsdesktopsmall,
                    itemsTablet: itemstablet,
                    itemsMobile: itemsmobile
                });
            }
        }
    });
})(jQuery);
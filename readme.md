# Developer Bundle

Version: 1.2

## Author:

SuperFastBusiness

## Summary

A collection of widgets and shortcodes for the Genesis Framework. The widgets is built using the modified [MP Core Widget Class](https://mintplugins.com/doc/widget-class/) from [Mint Plugins](http://mintplugins.com). Package Management is being handled by [Bower](http://bower.io). Scripts and stylesheets are compiled using [Gulp](http://gulpjs.com).

### Deployment

When developing on local environment(using XAMPP, MAMP, WAMP, EasyPHP) it is recommended to use this approach if not just clone/download the repository.

* Clone repository
* Install dependencies
    * [Ruby](http://rubyinstaller.org/)
    * [Node.js](https://nodejs.org/)
    * [Compass](http://compass-style.org)
    * [Breakpoint](http://breakpoint-sass.com/)
* Open Command Line and run `npm install` to install gulp and dependencies.
* Run `bower install` to install packages.
* Run `gulp` to compile scripts and stylesheet.

### Installation

- Download/clone from [bitbucket](https://webdevsuperfast@bitbucket.org/webdevsuperfast/widgets-by-superfastbusiness.git) repository.
- Copy folder to plugins folder and activate.
- Enjoy.

### Features

1. Widget Bundle
    * Image Widget
    * Button Widget
    * Masonry Widget
    * ~~Social Icon Widget~~
    * Tab Widget
    * Video Widget(now supports oEmbed and mp4 format videos)
    * Testimonial Widget
2. Shortcode Bundle
    * Image Shortcode
    * Wrapper Shortcode
    * Button Shortcode
    * Menu Shortcode
    * Text Shortcode
    * Span Shortcode
    * Font Awesome Shortcode
    * Content Box Shortcode
3. Built-in SASS/SCSS, Compass, Gulp, Bower support.
4. Resize images on the fly with [AQ Resizer](https://github.com/syamilmj/Aqua-Resizer)
5. Extensible Widget Class from [MP Core Widget Class](https://mintplugins.com/doc/widget-class/)
6. Update support using [Github Updater](https://github.com/afragen/github-updater)
7. Integrated Vafpress Framework support
8. Create Custom Post Type with ease using [Advanced Custom Post Types](https://github.com/kevindees/advanced_custom_post_types)

### Update

The Social Icon Widget have been removed. Please use other social icons plugins e.g. [Simple Social Icons](https://wordpress.org/plugins/simple-social-icons/). Proper documentation on the features will be added soon.

### Credits

Without these projects, this WordPress plugin wouldn't be made:

* [SASS / SCSS](http://sass-lang.com/)
* [Gulp](http://gulpjs.com/)
* [Bower](https://github.com/bower/bower)
* [Compass](http://compass-style.org)
* [AQ Resizer](https://github.com/syamilmj/Aqua-Resizer)
* [MP Core Widget Class](https://mintplugins.com/doc/widget-class/)
* [Packery](http://packery.metafizzy.co/)
* [EasyTabs](http://os.alfajango.com/easytabs/)
* [Owl Carousel](http://owlgraphic.com/owlcarousel/)
* [Vafpress Framework](http://vafpress.com/vafpress-framework/)